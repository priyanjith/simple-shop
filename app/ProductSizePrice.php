<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSizePrice extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products_size_prices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'size', 'price'
    ];


    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
