<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Session;
use DB;
use App\Product;
use App\ProductPhoto;
use App\Category;
use App\ProductPrice;

class CartController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
//        Session::forget('cart');
//        Session::flush();

        // get all the products
        $products = Product::all();
        $data = [
            'products' => $products,
        ];

        return view('welcome')->with($data);
    }

    public function addToCart(Request $request, $id){
        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id);
        $request->session()->put('cart',$cart);
        return Redirect::to('cart');
    }

    public function getCart(){
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return view('cart',['products' => $cart->products, 'totalPrice' => $cart->totalPrice]);
    }

    public function removeFromCart(Request $request, $id){
        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->remove($product, $product->id);
        $request->session()->put('cart',$cart);
        return Redirect::to('cart');
    }

}