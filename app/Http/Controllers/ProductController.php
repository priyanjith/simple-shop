<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Session;
use DB;
use App\Product;
use App\ProductPhoto;
use App\Category;
use App\ProductPrice;
use App\ProductSizePrice;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the products
        $products = Product::all();

        // load the view and pass the products
        return \View::make('products.index')
            ->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $allCategories = Category::pluck('title','id')->all();
        $sizeType = array(
            'S' => 'Small',
            'M' => 'Medium',
            'L' => 'Large'
        );
        $data = [
            'allCategories' => $allCategories,
            'sizeType' => $sizeType
        ];
        return view('products.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // validate
        $rules = array(
            'name' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('admin/products/create')
                ->withErrors($validator);
        } else {

            try {
                DB::beginTransaction();
                // store
                $product = new Product;
                $product->name = Input::get('name');
                $product->description = Input::get('description');
                $product->code = Input::get('code');
                $result = $product->save();

                if (count(Input::file('image')) && $result) {
                    foreach (Input::file('image') as $image) {
                        $fileName = time() . '-' . md5($image->getClientOriginalName());
                        $input['imagename'] = $fileName . '.' . $image->getClientOriginalExtension();
                        $destinationPath = public_path('/images/products/' . $product->id);
                        $image->move($destinationPath, $input['imagename']);
                        ProductPhoto::create([
                            'product_id' => $product->id,
                            'filename'   => $input['imagename'],
                            'filepath'   => '/images/products/' . $product->id . "/" . $input['imagename']
                        ]);
                    }
                }

                if ($result) {
                    $product->categories()->attach(Input::get('categories'));
                    //add price
                    if(count(Input::get('quantity'))) {
                        foreach (Input::get('quantity') as $key => $qty) {
                            $price = Input::get('price')[$key];
                            ProductPrice::create([
                                'product_id' => $product->id,
                                'qty'        => $qty,
                                'price'      => $price
                            ]);
                        }
                    }

                    //add size
                    if(count(Input::get('size'))) {
                        foreach (Input::get('size') as $key => $size) {
                            $price_size = Input::get('price_size')[$key];
                            ProductsizePrice::create([
                                'product_id' => $product->id,
                                'size'       => $size,
                                'price'      => $price_size
                            ]);
                        }
                    }
                }


                DB::commit();
                // redirect
                Session::flash('message', 'Successfully created product!');
                return Redirect::to('admin/products');
            }catch (\Exception $e){
                DB::rollback();
                return Redirect::to('admin/products/create')
                    ->withErrors($validator);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        $selectedCategories = $product->categories->pluck('title','id')->all();
        $prices = $product->prices;
        $photos = $product->photos;
        $sizes = $product->sizes;
        $sizeType = array(
            'S' => 'Small',
            'M' => 'Medium',
            'L' => 'Large'
        );
        $data = [
            'product' => $product,
            'selectedCategories' => $selectedCategories,
            'prices' => $prices,
            'photos' => $photos,
            'sizes' => $sizes,
            'sizeType' => $sizeType
        ];
        return view('products.view')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $allCategories = Category::pluck('title','id')->all();
        $product = Product::find($id);
        $selectedCategories = $product->categories->pluck('id')->all();
        $prices = $product->prices;
        $photos = $product->photos;
        $sizes = $product->sizes;
        $sizeType = array(
            'S' => 'Small',
            'M' => 'Medium',
            'L' => 'Large'
        );
        $data = [
            'allCategories' => $allCategories,
            'product' => $product,
            'selectedCategories' => $selectedCategories,
            'prices' => $prices,
            'photos' => $photos,
            'sizes' => $sizes,
            'sizeType' => $sizeType
        ];
        return view('products.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $product = Product::find($id);
        // validate
        $rules = array(
            'name' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('admin/products/edit/'.$id)
                ->withErrors($validator);
        } else {

            try {
                DB::beginTransaction();
                // store
                $product->name = Input::get('name');
                $product->description = Input::get('description');
                $product->code = Input::get('code');
                $result = $product->save();

                if (count(Input::file('image')) && $result) {
                    foreach (Input::file('image') as $image) {
                        $fileName = time() . '-' . md5($image->getClientOriginalName());
                        $input['imagename'] = $fileName . '.' . $image->getClientOriginalExtension();
                        $destinationPath = public_path('/images/products/' . $product->id);
                        $image->move($destinationPath, $input['imagename']);
                        ProductPhoto::create([
                            'product_id' => $product->id,
                            'filename'   => $input['imagename'],
                            'filepath'   => '/images/products/' . $product->id . "/" . $input['imagename']
                        ]);
                    }
                }

                if(count(Input::get('photos'))){
                    $photos = ProductPhoto::where('product_id',$id)->pluck('id')->all();
                    $diff = array_diff($photos, Input::get('photos'));
                    if(count($diff)){
                        ProductPhoto::destroy($diff);
                    }
                    //To do delete from the image folder
                }

                if ($result) {
                    $product->categories()->attach(Input::get('categories'));
                    //update price product price table
                    //deletes all before add

                    ProductPrice::where('product_id', '=', $id)->delete();
                    foreach (Input::get('quantity') as $key => $qty) {
                        $price = Input::get('price')[$key];
                        $productPrice = new ProductPrice;
                        $productPrice->product_id = $id;
                        $productPrice->qty = $qty;
                        $productPrice->price = $price;
                        $productPrice->save();
                    }
                    //deletes all before add
                    ProductSizePrice::where('product_id', '=', $id)->delete();
                    foreach (Input::get('size') as $key => $size) {
                        $price = Input::get('price_size')[$key];
                        $productPrice = new ProductSizePrice;
                        $productPrice->product_id = $id;
                        $productPrice->size = $size;
                        $productPrice->price = $price;
                        $productPrice->save();
                    }
                }
                DB::commit();
                // redirect
                Session::flash('message', 'Successfully edit product!');
                return Redirect::to('admin/products');
            }catch (\Exception $e){
                DB::rollback();
                Session::flash('error', 'Error edit product!');
                return Redirect::to('admin/products/edit/'.$id)
                    ->withErrors($validator);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $product = Product::find($id);
        $product->delete();

        //To Do
        //Remove image folder


        // redirect
        Session::flash('message', 'Successfully deleted the product!');
        return Redirect::to('admin/products');
    }
}
