<?php

namespace App;


class Cart
{
    public $products = null;
    public $totalQty = 0;
    public $totalPrice = 0;

    public function __construct($oldCart)
    {
        if($oldCart){
            $this->products = $oldCart->products;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    public function add($product, $id){
        $price = $product->prices->first()->price;
        $saveProduct = ['qty'=>1, 'price'=>$price,'product'=> $product];
        if($this->products){
            if(array_key_exists($id, $this->products )){
                $saveProduct = $this->products[$id];
            }
        }
        $this->products[$id] = $saveProduct;

        $saveProduct['price'] = $price * 1;
        $this->totalQty++;
        $this->totalPrice += $price;
    }

    public function remove($product, $id){
        $price = $product->prices->first()->price;
        $saveProduct = ['qty'=>1, 'price'=>$price,'product'=> $product];
        unset($this->products[$id]);
        $saveProduct['qty']--;
        $saveProduct['price'] = $price * $saveProduct['qty'];
        $this->totalQty--;
        $this->totalPrice -= $price;
    }

}
