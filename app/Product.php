<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'code'
    ];


    public function categories(){
        return $this->belongsToMany('App\Category', 'products_categories', 'product_id', 'category_id');
    }

    public function prices(){
        return $this->hasMany('App\ProductPrice', 'product_id', 'id', 'products_prices');
    }

    public function photos(){
        return $this->hasMany('App\ProductPhoto', 'product_id', 'id', 'products_prices');
    }

    public function sizes(){
        return $this->hasMany('App\ProductSizePrice', 'product_id', 'id', 'products_size_prices');
    }
}
