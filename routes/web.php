<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CartController@index')->name('landing');
Route::get('/cart/add/{id}', 'CartController@addToCart')->name('addToCart');
Route::get('/cart', 'CartController@getCart')->name('cart');
Route::get('/cart/remove/{id}', 'CartController@removeFromCart')->name('removeFromCart');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function() {
    Route::group(['prefix' => 'admin'], function () {
        $this->get('products', 'ProductController@index');
        $this->get('products/create', 'ProductController@create');
        $this->post('products/store', 'ProductController@store');
        $this->get('products/edit/{id}', 'ProductController@edit');
        $this->post('products/update/{id}', 'ProductController@update');
        $this->get('products/show/{id}', 'ProductController@show');
        $this->get('products/delete/{id}', 'ProductController@destroy');
        $this->get('categories', 'CategoryController@index');
        $this->get('categories/create', 'CategoryController@create');
        $this->post('categories/store', 'CategoryController@store');
        $this->get('categories/edit/{id}', 'CategoryController@edit');
        $this->post('categories/update/{id}', 'CategoryController@update');
        $this->get('categories/show/{id}', 'CategoryController@show');
        $this->get('categories/delete/{id}', 'CategoryController@destroy');

    });
});