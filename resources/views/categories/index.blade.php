@extends('adminlte::page')

@section('title', 'Product Manager')

@section('content_header')
    <h1>Category Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="pull-right">
                <a href="/admin/categories/create"><button type="button" class="btn btn-primary">Create</button></a>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-2">
                    <h3>Category List</h3>
                    <ul id="tree1">
                        @foreach($categories as $category)
                            <li >
                                {{ $category->title }}
                                @if(count($category->childs))
                                    @include('categories.child',['childs' => $category->childs])
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-10">
                    <table id="user-list" class="display responsive nowrap" width="100%">
                        <thead>
                        <tr >
                            <th class="col-md-3">Title</th>
                            <th class="col-md-3">Description</th>
                            <th class="col-md-3">Parent Category</th>
                            <th class="col-md-3">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr style="height:60px;">
                                <td  data-th="Title">{{ $category->title }}</td>
                                <td class="col-md-3" data-th="Description">{{ $category->description }}</td>
                                <td class="col-md-3" data-th="Parent Category">{{ isset($category->parent)? $category->parent->title : '' }}</td>
                                <td class="col-md-3" data-th="Actions">
                                    <div class="btn-group">
                                        <a href="/admin/categories/show/{{ $category->id }}">
                                            <button type="button" class="btn btn-primary">Show</button>
                                        </a>
                                        <a href="/admin/categories/edit/{{ $category->id }}">
                                            <button type="button" class="btn btn-info">Update</button>
                                        </a>
                                        <a href="/admin/categories/delete/{{ $category->id }}">
                                            <button type="button" class="btn btn-danger">Remove</button>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    @stop

@section('js')
    <script>
        $(document).ready(function() {

            $('#user-list').DataTable();
        } );

        $.fn.extend({
            treed: function (o) {

                var openedClass = 'glyphicon-minus-sign';
                var closedClass = 'glyphicon-plus-sign';

                if (typeof o != 'undefined'){
                    if (typeof o.openedClass != 'undefined'){
                        openedClass = o.openedClass;
                    }
                    if (typeof o.closedClass != 'undefined'){
                        closedClass = o.closedClass;
                    }
                };

                /* initialize each of the top levels */
                var tree = $(this);
                tree.addClass("tree");
                tree.find('li').has("ul").each(function () {
                    var branch = $(this);
                    branch.prepend("");
                    branch.addClass('branch');
                    branch.on('click', function (e) {
                        if (this == e.target) {
                            var icon = $(this).children('i:first');
                            icon.toggleClass(openedClass + " " + closedClass);
                            $(this).children().children().toggle();
                        }
                    })
                    branch.children().children().toggle();
                });
                /* fire event from the dynamically added icon */
                tree.find('.branch .indicator').each(function(){
                    $(this).on('click', function () {
                        $(this).closest('li').click();
                    });
                });
                /* fire event to open branch if the li contains an anchor instead of text */
                tree.find('.branch>a').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
                /* fire event to open branch if the li contains a button instead of text */
                tree.find('.branch>button').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
            }
        });
        /* Initialization of treeviews */
        $('#tree1').treed();
    </script>
@stop