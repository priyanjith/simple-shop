@extends('layouts.app')

@section('title', 'Cart')


@section('content')
    <div class="row">
        @foreach($products as $product)
            <div class="col-sm-4 col-md-2">
                <div class="thumbnail">

                    <img src="{{ isset($product->photos->first()->filepath) ? asset($product->photos->first()->filepath) : asset('/images/blank.jpg')}}" alt="..." style="max-height: 150px;" class="img-responsive">
                    <div class="caption">
                        <h3>{{ $product->name }}</h3>
                        <p>{{ $product->description }}</p>
                        <div class="clearfix">
                            <div class="pull-left price">{{ isset($product->prices->first()->price) ? ($product->prices->first()->price) : 0}}</div>
                            <a href="{{ route('addToCart', ['id' => $product->id]) }}" class="btn btn-success pull-right" role="button">Add to cart</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@stop

@section('css')
@stop

@section('js')

@stop