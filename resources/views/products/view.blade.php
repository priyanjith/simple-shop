@extends('adminlte::page')

@section('title', 'Product Manager')

@section('content_header')
    <h1>Product Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')

    <div class="form-group">
        <label for="name">Name</label>
        <p for="title">{{ isset($product) ? $product->name : "-" }}</p>
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <p for="title">{{ isset($product) ? $product->description : "-" }}</p>
    </div>

    <div class="form-group">
        <label for="categories">Categories</label>
        @foreach($selectedCategories as $category)
            <p>{{ $category }}</p>
        @endforeach
    </div>

    <div class="form-group">
        <label for="code">Code</label>
        <p for="title">{{ isset($product) ? $product->code : "-" }}</p>
    </div>

    <div class="form-group">
        <label for="image">Image</label>
        <div>
            @foreach($photos as $photo)
                <div class="img-wrap">
                    <span class="close">&times;</span>
                    <input type="hidden" name="photos[]" value="{{ $photo->id }}">
                    <img src="{{asset($photo->filepath)}}" data-id="{{ $photo->id }}" with="75px" height="75px">
                </div>
            @endforeach

        </div>
    </div>

    <div class="panel-body">
        <label for="prices">Quantity and Prices</label>
        <div id="education_fields">
        </div>


        @if(count($prices))
            @foreach($prices as $key => $price)
                <div class="form-group removeclass{{ $key }}">
                    <input type="hidden" name="price_id[]" value="{{ isset($price) ? $price->id : 0 }}">
                    <div class="col-sm-3 nopadding">
                        <div class="form-group">
                            <input type="text" class="form-control" id="quantity" name="quantity[]"
                                   value="{{ isset($price) ? $price->qty : 0 }}" placeholder="Quantity">
                        </div>
                    </div>
                    <div class="col-sm-3 nopadding">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" id="price" name="price[]"
                                       value="{{{ isset($price) ? $price->price : 0 }}}" placeholder="Price">
                                <div class="input-group-btn">
                                    @if($key)
                                        <button class="btn btn-danger" type="button"
                                                onclick="remove_education_fields(' + room + ');"><span
                                                    class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                        </button>
                                    @else
                                        <button class="btn btn-success" type="button"
                                                onclick="education_fields();"><span
                                                    class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            @endforeach
        @endif
    </div>


    {{--size with price--}}
    <div class="panel-body">
        <label for="prices">Size and Prices</label>
        <div id="education_fields_size">
        </div>
        @if(count($sizes))
            @foreach($sizes as $key => $size)
                <div class="form-group removeclass_size{{ $key }}">
                    <input type="hidden" name="price_size_id[]" value="{{ isset($size) ? $size->id : 0 }}">

                    <div class="col-sm-3 nopadding">
                        <div class="form-group">
                            {!! Form::select('size[]',$sizeType, $size->size , ['class'=>'form-control', 'placeholder'=>'Select Size']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3 nopadding">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" id="price_size" name="price_size[]" value="{{{ isset($size) ? $size->price : 0 }}}" placeholder="Price">
                                <div class="input-group-btn">
                                    @if($key)
                                        <button class="btn btn-danger" type="button" onclick="remove_education_fields_size(' + room + ');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button>
                                    @else
                                        <button class="btn btn-success" type="button" onclick="education_fields_size();"><span
                                                    class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            @endforeach
        @endif
    </div>

    <a href="/admin/products">
        <button type="button" class="btn btn-info">Back</button>
    </a>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
@section('js')
@stop