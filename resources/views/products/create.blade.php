@extends('adminlte::page')

@section('title', 'Product Manager')

@section('content_header')
    <h1>Product Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')
    {{  Form::open(array('url'=>'admin/products/store', 'method' => 'post', 'enctype' => 'multipart/form-data')) }}
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" class="form-control" placeholder="Name">
        @if ( $errors->has('name') )
            <span class="text-danger">{{{ $errors->first('name') }}}</span>
        @endif
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea type="text" name="description" class="form-control" placeholder="Description"></textarea>
    </div>

    <div class="form-group">
        <label for="categories">Categories</label>
        {{Form::select('categories[]',$allCategories,null,array('multiple'=>'multiple', 'class'=>'form-control',))}}
        @if ( $errors->has('categories') )
            <span class="text-danger">{{{ $errors->first('categories') }}}</span>
        @endif
    </div>

    <div class="form-group">
        <label for="code">Code</label>
        <input type="text" name="code" autocomplete="off" class="form-control" placeholder="Code">
        @if ( $errors->has('code') )
            <span class="text-danger">{{{ $errors->first('code') }}}</span>
        @endif
    </div>

    <div class="form-group">
        <label for="image">Image</label>
        <input type="file" class="form-control" name="image[]" placeholder="Upload Image" multiple="true">
    </div>


    <div class="panel panel-default">
        <div class="panel-body">
            <label for="prices">Quantity and Prices</label>
            <div id="education_fields">

            </div>
            <div class="col-sm-3 nopadding">
                <div class="form-group">
                    <input type="text" class="form-control" id="quantity" name="quantity[]" value="1"
                           placeholder="Quantity">
                </div>
            </div>
            <div class="col-sm-3 nopadding">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" id="price" name="price[]" value="1.00"
                               placeholder="Price">
                        <div class="input-group-btn">
                            <button class="btn btn-success" type="button" onclick="education_fields();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <label for="prices">Size and Prices</label>
            <div id="education_fields_size">

            </div>
            <div class="col-sm-3 nopadding">
                <div class="form-group">
                    {!! Form::select('size[]',$sizeType,'' , ['class'=>'form-control', 'placeholder'=>'Select Size']) !!}
                </div>
            </div>
            <div class="col-sm-3 nopadding">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" id="price_size" name="price_size[]" value="1.00"
                               placeholder="Price">
                        <div class="input-group-btn">
                            <button class="btn btn-success" type="button" onclick="education_fields_size();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
    {{ Form::close() }}

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        var room = 1;
        function education_fields() {

            room++;
            var objTo = document.getElementById('education_fields')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "form-group removeclass" + room);
            var rdiv = 'removeclass' + room;
            divtest.innerHTML = '<div class="col-sm-3 nopadding"><div class="form-group"> <input type="text" class="form-control" id="quantity" name="quantity[]" value="" placeholder="Quantity"></div></div><div class="col-sm-3 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control" id="price" name="price[]" value="" placeholder="Price"><div class="input-group-btn"> <button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div></div><div class="clear"></div>';

            objTo.appendChild(divtest)
        }
        function remove_education_fields(rid) {
            $('.removeclass' + rid).remove();
        }


        //size
        var room_size = 1;
        function education_fields_size() {

            room_size++;
            var objTo = document.getElementById('education_fields_size')
            var divtest = document.createElement("div_size");
            divtest.setAttribute("class", "form-group removeclass_size" + room_size);
            var rdiv = 'removeclass_size' + room_size;
            divtest.innerHTML = '<div class="col-sm-3 nopadding"><div class="form-group"><select class="form-control" name="size[]"><option selected="selected" disabled="disabled" hidden="hidden" value="">Select Size</option><option value="S">Small</option><option value="M">Medium</option><option value="L">Large</option></select></div></div><div class="col-sm-3 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control" id="price_size" name="price_size[]" value="" placeholder="Price"><div class="input-group-btn"> <button class="btn btn-danger" type="button" onclick="remove_education_fields_size(' + room_size + ');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div></div><div class="clear"></div>';

            objTo.appendChild(divtest)
        }
        function remove_education_fields_size(rid) {
            $('.removeclass_size' + rid).remove();
        }
    </script>

@stop